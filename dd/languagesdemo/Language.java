package dd.languagesdemo;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Language {
	private static void run(String script, Languages language)
			throws ScriptException {
		ScriptEngineManager manager = new ScriptEngineManager();
		String engineLanguage = getLanguage(language);
		ScriptEngine engine = manager.getEngineByName(engineLanguage);
		engine.eval(script);
	}

	public static String getLanguage(Languages language) {
		switch (language) {
		case jruby:
			return "jruby";
		case groovy:
			return "groovy";
		case jython:
			return "python";
		}

		return "java";
	}
	
	public static void runScript(String script, Languages lang) throws ScriptException{
		if(lang != Languages.java){
			System.out.println(getLanguage(lang));
			System.out.println("CODE\n"+script+"\nRESULT");
			Language.run(script, lang);
		}
	}
}
