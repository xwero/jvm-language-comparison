package dd.demos;

import javax.script.ScriptException;

import dd.languagesdemo.Language;
import dd.languagesdemo.Languages;

public class Arrays {
	public static void create(Languages language, boolean java) throws ScriptException{
		String script = "";
		String scriptJava = "";
		
		switch(language){
			case java:
				String[] presidents = new String[6];
				
				System.out.println(presidents.length);
				break;
			case jruby:
				script = 
					"presidents = []\n"+
					"puts presidents.length";
				scriptJava = 
					"presidents = Array.new(6)\n"+
					"puts presidents.length";
				break;
			case groovy:
				script = 
					"def presidents = []\n"+
					"println presidents.length";
				break;
			case jython:
				script =
					"presidents = []\n"+
					"print len(presidents)";
				scriptJava =
					"presidents = [None] *6\n"+
					"print len(presidents)";
		}
		
		script = checkScript(java, script, scriptJava);
		
		Language.runScript(script, language);
	}

	public static void createAndFill(Languages language) throws ScriptException{
		String script = "";
		
		switch(language){
			case java:
				String[] presidents = {"Ford", "Carter", "Reagan", "Bush1", "Clinton", "Bush2"};
				
				System.out.println(presidents.length);
				break;
			case jruby:
				script = 
					"presidents = ['Ford', 'Carter', 'Reagan', 'Bush1', 'Clinton', 'Bush2']"+
					"puts presidents.length";
				break;
			case groovy:
				script = 
					"def presidents = ['Ford', 'Carter', 'Reagan', 'Bush1', 'Clinton', 'Bush2']"+
					"println presidents.length";
				break;
			case jython:
				script = 
					"presidents = ['Ford', 'Carter', 'Reagan', 'Bush1', 'Clinton', 'Bush2']"+
					"print len(presidents)";
		}
		
		Language.runScript(script, language);
	}
	
	public static void createAndFillLater(Languages language, boolean java) throws ScriptException{
		String script = "";
		String scriptJava = "";
		
		switch(language){
			case java:
				String[] presidents = new String[6];
				
				presidents[0] = "Ford";
				presidents[1] = "Carter";
				presidents[2] = "Reagan";
				presidents[3] = "Bush1";
				presidents[4] = "Clinton";
				presidents[5] = "Bush2";
				break;
			case jruby:
				script = 
					"presidents = []\n"+
					"presidents << 'Ford' << 'Carter' << 'Reagan' << 'Bush1' << 'Clinton' << 'Bush2'\n"+
					"puts presidents";
				scriptJava = 
					"presidents = Array.new(6)\n"+
					"presidents[0] = 'Ford'\n"+
					"presidents[1] = 'Carter'\n"+
					"presidents[2] = 'Reagan'\n"+
					"presidents[3] = 'Bush1'\n"+
					"presidents[4] = 'Clinton'\n"+
					"presidents[5] = 'Bush2'\n"+
					"puts presidents";
				break;
			case groovy:
				script = 
					"def presidents = []\n"+
					"presidents << 'Ford' << 'Carter' << 'Reagan' << 'Bush1' << 'Clinton' << 'Bush2'\n"+
					"println presidents";
				scriptJava = 
					"def presidents = []\n"+
					"presidents[0] = 'Ford'\n"+
					"presidents[1] = 'Carter'\n"+
					"presidents[2] = 'Reagan'\n"+
					"presidents[3] = 'Bush1'\n"+
					"presidents[4] = 'Clinton'\n"+
					"presidents[5] = 'Bush2'\n"+
					"println presidents";
				break;
			case jython:
				script = 
					"presidents = []\n"+
					// an array needs to be injected to have a concise adding of items
					"presidents[0:6] = ['Ford', 'Carter', 'Reagan', 'Bush1', 'Clinton', 'Bush2']\n"+
					"print presidents";
				scriptJava = 
					"presidents = []\n"+
					"presidents.append('Ford')\n"+
					"presidents.append('Carter')\n"+
					"presidents.append('Reagan')\n"+
					"presidents.append('Bush1')\n"+
					"presidents.append('Clinton')\n"+
					"presidents.append('Bush2')\n"+
					"print presidents";
		}
		
		script = checkScript(java, script, scriptJava);
		
		Language.runScript(script, language);
	}
	
	public static void forLoop(Languages language, boolean alternative) throws ScriptException{
		String script = "";
		String scriptAlternative = "";
		
		switch(language){
			case java:
				String[] presidents = {"Ford", "Carter", "Reagan", "Bush1", "Clinton", "Bush2"};
				
				for(int i = 0,max = presidents.length;i<max;i++){
					System.out.println(presidents[i]);
				}
				break;
			case jruby:
				script = 
					"presidents = ['Ford', 'Carter', 'Reagan', 'Bush1', 'Clinton', 'Bush2']\n"+
					// the three points exclude the presidents.length number from the range
					"for i in 0...presidents.length\n"+
					"puts presidents[i]\n"+
					"end";
				scriptAlternative = 
					"presidents = ['Ford', 'Carter', 'Reagan', 'Bush1', 'Clinton', 'Bush2']\n"+
					"presidents.length.times do |i|\n"+
					"puts presidents[i]\n"+
					"end";
				break;
			case groovy:
				script = 
					"def presidents = ['Ford', 'Carter', 'Reagan', 'Bush1', 'Clinton', 'Bush2']\n"+
					"for(i in 0..<presidents.size){\n"+
					"println presidents[i]\n"+
					"}";
				break;
			case jython:
				script = 
					"presidents = ['Ford', 'Carter', 'Reagan', 'Bush1', 'Clinton', 'Bush2']\n"+
					"for i in range(len(presidents)):\n"+
					"    print presidents[i]";
		}
		
		script = checkScript(alternative, script, scriptAlternative);
		
		Language.runScript(script, language);
	}
	
	public static void forEachLoop(Languages language) throws ScriptException{
		String script = "";
		
		switch(language){
			case java:
				String[] presidents = {"Ford", "Carter", "Reagan", "Bush1", "Clinton", "Bush2"};
				
				for(String president : presidents){
					System.out.println(president);
				}
				break;
			case jruby:
				script = 
					"presidents = ['Ford', 'Carter', 'Reagan', 'Bush1', 'Clinton', 'Bush2']\n"+
					"presidents.each do |president|\n"+
					"puts president\n"+
					"end";
				break;
			case groovy:
				script = 
					"def presidents = ['Ford', 'Carter', 'Reagan', 'Bush1', 'Clinton', 'Bush2']\n"+
					"presidents.each{\n"+
					"println it\n"+
					"}";
				break;
			case jython:
				script = 
					"presidents = ['Ford', 'Carter', 'Reagan', 'Bush1', 'Clinton', 'Bush2']\n"+
					"for president in presidents:\n"+
					"    print president";
		}
		
		Language.runScript(script,language);
	}
	
	private static String checkScript(boolean flag, String script,
			String flagScript) {
		return flag == true && !flagScript.isEmpty() ? flagScript : script;
	}
}
